# shieldsup-django-geodata

This repo includes GIS datasets that are used by [shieldsup-django](https://gitlab.com/shieldsupnz/shieldsup-django) to calculate regions.

The datasets are in Shapefile format with WGS-84 projection

Currently the following datasets are included:
- district-health-board-2015
- regional-council-2020-generalised
- auckland-council-boards-july-2010
- territorial-authority-2020-generalised

## License

This work is based on/includes Stats NZ’s data which are licensed by Stats NZ for reuse under the Creative Commons Attribution 4.0 International licence.
